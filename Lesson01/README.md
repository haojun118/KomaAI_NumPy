小马AI课程总索引
===========

## 概要

对于AI时代，想转型为AI工程师的朋友，一起来学习AI课程吧。

## 课程内容

### 入门：数据科学

+ Numpy
+ Scipy
+ Pandas
+ Matplotlib
+ OpenCV
+ scikit-learn

### 进阶：深度强化学习

+ Keras
  - Tensorflow
+ OpenAI
  - gym
  - retro

## 基础知识

+ Python3编程基础

## 开发工具

* 记事本等文本编辑器
 1. Visual Studio Code(推荐)
 2. Visual Studio Community 2019
 3. PyCharm

## 视频计划
* 每个视频只包括一个知识点，并控制在5-10分钟之内
* 原代码共享
  - Gitee@OSC  
    https://gitee.com/komavideo

## 安装

```bash
$ pip install numpy
$ pip install scipy
$ pip install pandas
$ pip install matplotlib
$ pip install opencv-python
$ pip install scikit-learn
```

## 小马视频频道

http://komavideo.com
